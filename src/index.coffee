console.log "Hello there! It's nice that you are looking under the hood. The source code for this page is here: https://gitlab.com/hornbook/hornbook.gitlab.io/"

query = new URLSearchParams location.search

if query.has "sent"
    document.body.classList.add "message-sent"
